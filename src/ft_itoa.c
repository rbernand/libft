/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 19:03:30 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/03 19:18:47 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>

char			ft_newchar(long nb, int base)
{
	if (base <= 10)
		return (nb + '0');
	else if (base <= 36)
	{
		if (nb < 10)
			return (nb + '0');
		else
			return (nb + 'A' - 10);
	}
	return ('#');
}

char			*ft_itoa(int nb, int base)
{
	static char		s[35];
	int				sign;
	int				i;

	ft_bzero(s, 35);
	if (nb == 0)
		return ("0");
	sign = nb < 0 ? 1 : 0;
	nb = nb < 0 ? -nb : nb;
	i = 33;
	while (i && nb)
	{
		s[i--] = ft_newchar(nb % base, base);
		nb /= base;
	}
	if (sign)
		s[i--] = '-';
	return (s + i + 1);
}
