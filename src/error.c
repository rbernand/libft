/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/11 13:43:39 by rbernand          #+#    #+#             */
/*   Updated: 2015/05/27 14:40:52 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_return	msg_error(const char *msg, const char *f, const char *func, int n)
{
	ft_putstr("\033[31;4mError: \033[0m");
	ft_putstr(f);
	ft_putstr(" - ");
	ft_putstr(func);
	ft_putstr(":");
	ft_putnbr(n);
	ft_putstr("\n\t");
	ft_putendl(msg);
	return (_ERR);
}
