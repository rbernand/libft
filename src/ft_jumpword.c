/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_jumpword.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/17 16:50:46 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/17 18:07:35 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

const char				*ft_jumpword(const char *str)
{
	size_t			i;

	i = 0;
	while (ft_iswhite(str[i]))
		i++;
	while (ft_isalnum(str[i]))
		i++;
	while (ft_iswhite(str[i]))
		i++;
	return (str + i);
}
